<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Moringa Core Assignment One</title>

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="{{ URL::asset('public/js/jquery-1.11.1.min.js') }}"></script>

        <!-- Style -->
        <link href="{{ URL::asset('public/css/style.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <h2>Let's Play!</h2>
                <h4>Rules:</h4>
                <p>Ping Pong will count up your number from 1 with the following exceptions:</p>
                <ul>
                    <li>Numbers divisible by 3 become "ping"</li>
                    <li>Numbers divisible by 5 become "pong"</li>
                    <li>Numbers divisible by 3 and 5 become "ping pong"</li>
                </ul>
                <p>Enter your number here:</p>
                <input class="value" type="text" name="" id="input_number" value="">
                <input class="submit" type="button" id="submit" value="Ping Pong!"/>
                <input class="reset" type="button" id="reset" value="Reset!"/>
                <p class="info">Developed by Yommie Samora</p>

                <div class="links">
                    <div id="output"></div>
                </div>
            </div>
        </div>

        <!-- Javascript Functions Here -->
        <script src="{{ URL::asset('public/js/custom.js') }}"></script>
    </body>
</html>
