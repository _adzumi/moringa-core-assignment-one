# Moringa Core Assignment One

On this project I was tasked with creating a web application that allows user to input a random number and returns all numbers between that number and 1.
It also replaces numbers divisible by 3 with 'ping', numbers divisible by 5 with 'pong' and numbers divisible by both 3 and 5 with 'pingpong'.

## Getting Started

I developed this application using the [Laravel Framework](https://laravel.com/docs) together with jQuery to compliment some of the features and functionalities required for the application to function.

### Requirements

Before trying to deploy a Laravel application on a shared hosting, you need to make sure that the hosting services provide a fit requirement to Laravel. Basically, following items are required for Laravel 5.4:

- PHP >= 5.5.9
- OpenSSL PHP Extension
- PDO PHP Extension

Besides PHP and those required extensions, you might need some utilities to make deployment much easier.
- [Git](https://git-scm.com/)
- [Composer](https://getcomposer.org/)

### Installing

- Clone this repository to a location on your file system.
- `cd` into the directory, run `php artisan serve` to start the server.
- Navigate to `localhost:8000` in your browser.

### Break down of Application

Ok, it's simple. Once the user inputs a random number, the application first validates the value i.e checks if its a valid number.

```
var number = $("#input_number").val();

if(number=="" || isNaN(parseFloat(number)) || number==0) { 
    alert("Please enter a valid number.");
} 
```

Next, it simply takes the number and displays every number between 1 and that number while also checking for divisibility by 3, 5 or both.

```
for(i = 1; i <= number; i++){
    if (i % 3 == 0) {
        if (i % 15 == 0) {
            document.getElementById("output").innerHTML += ("<p>pingpong</p>"); 
        }
        else {
            document.getElementById("output").innerHTML += ("<p>ping</p>");                 
        }
    }
    else if (i % 5 == 0) {
        if (i % 15 == 0) {
            document.getElementById("output").innerHTML += ("<p>pingpong</p>");
        }
        else {
            document.getElementById("output").innerHTML += ("<p>pong</p>");                 
        }
    }
    else if (i % 15 == 0){
        document.getElementById("output").innerHTML += ("<p>pingpong</p>"); 
    } 
    else {
        document.getElementById("output").innerHTML += ("<p> " +i+ " </p>");
    }   
}
```

