jQuery(document).ready(function() {

    "use strict";  

    $("#reset").hide();

    $("#submit").click(function(){
        var number = $("#input_number").val();
        var i;
        document.getElementById("output").innerHTML = ("<p></p>"); 

        if(number=="" || isNaN(parseFloat(number)) || number==0) { 
            alert("Please enter a valid number.");
            $("#input_number").val("");
            $("#input_number").focus();
            $("#reset").hide();
            $("#submit").show();
            return false; 
        } 

        for(i = 1; i <= number; i++){
            if (i % 3 == 0) {
                if (i % 15 == 0) {
                    document.getElementById("output").innerHTML += ("<p>pingpong</p>"); 
                }
                else {
                    document.getElementById("output").innerHTML += ("<p>ping</p>");                 
                }
            }
            else if (i % 5 == 0) {
                if (i % 15 == 0) {
                    document.getElementById("output").innerHTML += ("<p>pingpong</p>");
                }
                else {
                    document.getElementById("output").innerHTML += ("<p>pong</p>");                 
                }
            }
            else if (i % 15 == 0){
                document.getElementById("output").innerHTML += ("<p>pingpong</p>"); 
            } 
            else {
                document.getElementById("output").innerHTML += ("<p> " +i+ " </p>");
            }   
        }

        $("#reset").show();                                                               
    });

    $("#reset").click(function() {
        document.getElementById("output").innerHTML = ("<p></p>"); 
        $("#input_number").val("");
        $("#reset").hide();
        $("#submit").show();
    });

});